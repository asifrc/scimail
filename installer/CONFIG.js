exports.settings = {
/**
*	TARGET DIRECTORY
*	----------------
*	The directory you wish to monitor (will be created if it does not exist)
*	Use forward slashes, e.g.
*		CORRECT
*			"C:/Path/To/Files"
*		INCORRECT
*			"C:\Path\To\Files"
*/

	"directory": "C:/example_dir",

/**
*	ARCHIVE DIRECTORY
*	-----------------
*	The dirctory to which the file will be moved after being emailed
*	Make sure to use forward slashes as noted above
*/

	"archive": "C:/example_dir/archive",

/**
*	SMTP AUTHENTICATION
*	-------------------
*	SMTP settings for outgoing emails sent by the application
*/

	"service": "Gmail",
	"user": "nodemailer@asifchoudhury.com",
	"password": "nodemailer",
	
/**
*	EMAIL SETTINGS
*	--------------
*	Customize the automated email sent by the application
*/

	"to": "scimailtest@asifchoudhury.com",
	"from": "Report Mailing Service <no-reply@asifchoudhury.com>",
	"replyTo": "testenginering@asifchoudhury.com",
	"subject": "Daily SCI Product Test Report",
	"appendDate": true, // Adds " - m/d/y " to subject line
	"body": "Today's reports are attatched.\n\nThe Engineering Team",
	
/**
*	EMAIL FREQUENCY
*	---------------
*	Provide a valid string describing the frequency at which to mail files
*	Uses the following format
*		"* * * * * *"
*		 | | | | | | 
*		 | | | | | +-- Day of the Week   (range: 1-7, 1 standing for Monday)
*		 | | | | +---- Month of the Year (range: 1-12)
*		 | | | +------ Day of the Month  (range: 1-31)
*		 | | +-------- Hour              (range: 0-23)
*		 | +---------- Minute            (range: 0-59)
*		 +------------ Second            (range: 0-59)
*/

	"frequency": "0 0 18 * * *" //Runs Everyday at 6pm (18:00:00)

};