
/**
 * Module dependencies.
 */

var cron = require('cron')
	, fs = require('fs')
	, nodemailer = require('nodemailer')
	, config = require('./CONFIG.js')
	, cfg = config.settings;

//Cron Job to send out emails
var cronJob = cron.CronJob;
new cronJob(cfg.frequency, function() {
	console.log("Starting CronJob"); //DEBUG
	//Set up smtp transport
	var smtp = nodemailer.createTransport("SMTP", {
		service: cfg.service,
		auth: {
			user: cfg.user,
			pass: cfg.password
		}
	});
	//Set up email object
	var d = new Date();
	var email = {
		from: cfg.from,
		to: cfg.to,
		subject: (cfg.appendDate) ? cfg.subject+" - "+(parseInt(d.getMonth())+1)+"/"+d.getDate()+"/"+d.getFullYear() : cfg.subject,
		text: cfg.body,
		attachments: []
	};
	
	//Populate Attachments
	var att = [];
	
	//create monitoring directory if it doesn't exist
	if (!fs.existsSync(cfg.directory))
	{
		fs.mkdirSync(cfg.directory);
	}
	//Get the contents of the monitoring directory
	var dir = fs.readdirSync(cfg.directory);
	//Loop through each item in the directory
	for (var i=0; i<dir.length; i++)
	{
		//Check if the item is a file
		var stats = fs.statSync(cfg.directory+"/"+dir[i]);
		if (stats.isFile())
		{
			//Add file to attachments array
			att.push(dir[i]);
		}
	}
	
	//Add attachments to email object
	for (var j=0; j<att.length; j++)
	{
		email.attachments.push({
			filePath: cfg.directory+"/"+att[j]
		});
	}
	
	//Send email
	console.log("Sending email..");
	console.log(email); //DEBUG
	smtp.sendMail(email, function(error, response){
		//Close SMTP connection
		smtp.close();
		console.log("Email report sent."); //DEBUG

		//Create archive directory if it doesn't exist
		if (!fs.existsSync(cfg.archive))
		{
			fs.mkdirSync(cfg.archive);
		}
		var archname, testname, archcnt, archvalid, ext;
		//Loop through each attachment and move it to the archives folder
		for (var k=0; k<att.length; k++)
		{
			archname = cfg.archive+"/"+att[k];
			archcnt = 0;
			archvalid = false;
			//Check if file with same name already exists in archive
			if (fs.existsSync(archname))
			{
				//Increment the appended number until the file name is unique
				while (!archvalid)
				{
					archcnt++;
					testname = archname+"("+archcnt+")";
					if (ext = archname.match(/[\.]{1}[^\.]*$/))
					{
						testname = archname.replace(/[\.]{1}[^\.]*$/, "("+archcnt+")"+ext);
					}
					if (!fs.existsSync(testname))
					{
						//Set the archive name to the unique filename
						archname = testname;
						archvalid = true;
					}
				}
			}
			//Move the file to the archive location
			console.log("ARCHIVE: "+att[k]+" moved to "+archname);
			fs.renameSync(cfg.directory+"/"+att[k], archname);
		}
	});
}, null, true);

console.log("MRCmail v0.9.1 started...");